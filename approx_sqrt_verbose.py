# Approximates the square root of "num"
# This VERBOSE version shows how the number changes over time
# Optionally: Define the error (ie. per-guess difference) as a terminating condition
def approxSqrtVerbose(num, error=0.00001):
	print("Approximating sqrt({}) to an error of {}".format(num, error))

	guess = num					# First guess is just num itself
	diff = 999999999			# diff is used to check terminating condition
	count = 1					# Maintain count
	
	while diff > error:
		# Apply Newton's Method
		newGuess = guess - ((guess**2 - num) / (2*guess))
		
		# Calculate the absolute difference between the two guesses
		diff = newGuess - guess
		if diff < 0:
			diff *= -1
			
		# Update existing guess
		guess = newGuess
		
		# Show result
		print("\tGuess #{}: {} (diff: {})".format(count, guess, diff))
		count += 1
		
	return guess
	
while True:
	num = input("Enter value (or non-numerical value to quit): ")
	try:
		num = float(num)
	except:
		print("Goodbye!")
		break
		
	ans = approxSqrtVerbose(num)
	print("Final Answer - sqrt({}) is approximately {}\n\n".format(num,ans))