# Approximates the square root of "num"
# Optionally: Define the error (ie. per-guess difference) as a terminating condition
def approxSqrt(num, error=0.00001):
	guess = num					# First guess is just num itself
	diff = 999999999			# diff is used to check terminating condition
	while diff > error:
		# Apply Newton's Method
		newGuess = guess - ((guess**2 - num) / (2*guess))
		
		# Calculate the absolute difference between the two guesses
		diff = newGuess - guess
		if diff < 0:
			diff *= -1
			
		# Update existing guess
		guess = newGuess
		
	return guess
	
print(approxSqrt(427))