# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Approximating Square Roots w/ Newton's Method**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/tUFzOLDuvaE/0.jpg)](https://www.youtube.com/watch?v=tUFzOLDuvaE "Click to Watch")


# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.